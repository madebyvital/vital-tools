<?php
/**
 * Plugin Name: Vital Tools
 * Plugin URI: https://bitbucket.org/madebyvital/vital-tools/src/main/
 * Description: Vital functions, helpers, and customizations.
 * Version: 2.1.6
 * Requires PHP: 7.4
 * Requires at least: 6.1.0
 * Tested up to: 6.7.2
 * Author: Vital
 * Author URI: https://vitaldesign.com/
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: vital-tools
 * Domain Path: /languages
 * Bitbucket Plugin URI: https://bitbucket.org/madebyvital/vital-tools/src/main/
 * Bitbucket Branch: main
*/

if (!defined('ABSPATH')) {
	exit;
}

require 'plugin-update-checker/plugin-update-checker.php';

$update_checker = Puc_v4_Factory::buildUpdateChecker(
	'https://bitbucket.org/madebyvital/vital-tools',
	__FILE__,
	'vital-tools'
);

if (!class_exists('Vital_Tools')) {

	/**
	 * Main plugin class.
	 */
	#[AllowDynamicProperties]
	class Vital_Tools {

		/**
		 * The single instance of Vital_Tools.
		 *
		 * @var    object
		 * @access private
		 * @since  1.0.0
		 */
		private static $instance;

		/**
		 * The main plugin file.
		 *
		 * @var    string
		 * @access public
		 * @since  1.0.0
		 */
		public $file;

		/**
		 * Gets class instance.
		 *
		 * @access public
		 * @since  1.0.0
		 * @return Vital_Tools
		 */
		public static function instance() {
			if (!isset(self::$instance) && !(self::$instance instanceof Vital_Tools)) {
				self::$instance = new Vital_Tools;
				self::$instance->includes();
				self::$instance->init = new Vital_Tools_Init();
			}
			return self::$instance;
		}

		/**
		 * Sets up the class functionality.
		 *
		 * @access public
		 * @since  1.0.0
		 * @return void
		 */
		public function __construct() {
			$this->file = plugin_dir_path(__FILE__);
			add_action('plugins_loaded', [$this, 'load_plugin_textdomain']);
			add_action('admin_enqueue_scripts', [$this, 'vtl_enqueue_custom_search_script']);
		}

		/**
		 * Register and enqueue a custom scripts
		 */
		public function vtl_enqueue_custom_search_script() {
			wp_enqueue_script('vtl_custom_search_script_js', plugins_url('js/nav-menu-search.js', __FILE__), ['jquery']);
			wp_localize_script('vtl_custom_search_script_js', 'vtl_search_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
		}

		/**
		 * Includes required plugin files.
		 *
		 * @access private
		 * @since  1.0.0
		 * @return void
		 */
		private function includes() {

			// Required plugin components
			require_once($this->file . 'includes/plugin-helpers.php');

			// Plugin classes
			require_once($this->file . 'includes/class-acf.php');
			require_once($this->file . 'includes/class-admin-blog-rename.php');
			require_once($this->file . 'includes/class-admin-nav-search.php');
			require_once($this->file . 'includes/class-admin-render.php');
			require_once($this->file . 'includes/class-admin-reveal-ids.php');
			require_once($this->file . 'includes/class-admin-users.php');
			require_once($this->file . 'includes/class-development.php');
			require_once($this->file . 'includes/class-gravity-forms.php');
			require_once($this->file . 'includes/class-media-library.php');
			require_once($this->file . 'includes/class-oembed.php');
			require_once($this->file . 'includes/class-page-cleanup.php');
			require_once($this->file . 'includes/class-page-render.php');
			require_once($this->file . 'includes/class-performance.php');
			require_once($this->file . 'includes/class-rename-blog.php');
			require_once($this->file . 'includes/class-search.php');
			require_once($this->file . 'includes/class-searchwp.php');
			require_once($this->file . 'includes/class-theme-setup.php');
			require_once($this->file . 'includes/class-wordpress.php');
			require_once($this->file . 'includes/class-yoast.php');
			require_once($this->file . 'includes/class-disable-file-editors.php');

			// Initialize classes
			require_once($this->file . 'includes/class-vital-tools-init.php');

			// Plugin libraries
			require_once($this->file . 'includes/lib/acf.php');
			require_once($this->file . 'includes/lib/development.php');
			require_once($this->file . 'includes/lib/gravity-forms.php');
			require_once($this->file . 'includes/lib/helpers.php');
			require_once($this->file . 'includes/lib/yoast.php');
		}

		/**
		 * Loads the plugin text domain.
		 *
		 * @access public
		 * @since  1.0.0
		 * @return void
		 */
		public function load_plugin_textdomain() {
			load_plugin_textdomain('vital-tools', false, basename(dirname(__FILE__)) . '/languages/');
		}
	}

	/**
	 * Initializes Vital_Tools.
	 *
	 * @access public
	 * @since  1.0.0
	 * @return Vital_Tools
	 */
	function vital_tools_run() {
		return Vital_Tools::instance();
	}

	vital_tools_run();
}
