![Logo](assets/vital-logo.png)

# Vital Tools for WordPress

## Description

Vital functions, helpers, and customizations plugin for WordPress.

- **Contributors:**       Adam Walter, Matthew Chase, Dan Mensinger, Ram Velusamy, Bhavan Aditya
- **Tags:**               vital-tools
- **Requires at least:**  ```6.1.0```
- **Tested up to:**       ```6.7.2```
- **Stable tag:**         ```2.1.6```
- **Requires PHP:**       ```7.4```
- **Author:**             Vital
- **Author URI:**         https://vitaldesign.com/
- **License:**            GPLv2 or later
- **License URI:**        https://www.gnu.org/licenses/gpl-2.0.html


### Requirements

- WordPress 6.1.0 or later
- PHP 7.4 or later

---

## Installation

### Installing via the WordPress Dashboard

1. **Download the Plugin:**
   - Clone the repository:
     ```bash
     git clone https://bitbucket.org/madebyvital/vital-tools.git
     ```

2. **Upload the Plugin:**
   - In your WordPress Admin dashboard, navigate to `Plugins > Add New > Upload Plugin`.
   - Upload the zipped plugin folder.

3. **Activate the Plugin:**
   - Once uploaded, go to `Plugins > Installed Plugins` and activate "Vital Tools."

4. **Configure the Plugin:**
   - Head to `Vital Tools` to input your old and new URLs.


### Manual Installation

1. Download the plugin from the repository.
2. Extract the plugin files.
3. Upload the folder to the `/wp-content/plugins/` directory via FTP.
4. Activate the plugin from the WordPress admin panel.

---

## ⚖️ License
This plugin is licensed under the [GPL-2.0+](https://www.gnu.org/licenses/gpl-2.0.html).

---

## ❤️ Contribute
Feel free to contribute or report issues on [Bitbucket](https://bitbucket.org/madebyvital/skeletor-userback/).

---

## Change log

### 2.1.6
- Disable the file editor in the backend

[See change log for all versions](https://bitbucket.org/madebyvital/vital-tools/src/main/changelog.md).

---
