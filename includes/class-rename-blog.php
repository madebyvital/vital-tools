<?php

if (!defined('ABSPATH')) {
	exit;
}


/**
 * Renames core "Posts" post type to "Blog Posts" throughout WordPress.
 *
 * uses quite similar functionality to Vital-Tools/includes/class-admin-blog-rename.php
 * defined here as a stand-alone/fallback in case vital-tools
 * is not installed or de-activated
 *
 * @since 2.1.4
 */

class Vital_Rename_Blog {

	/**
	 * Sets up the class functionality.
	 *
	 * @access public
	 * @since  2.1.4
	 * @return void
	 */
	public function __construct() {
		add_action('init', [$this, 'change_post_object_labels']);
		add_action('admin_menu', [$this, 'change_admin_menu_text']);
	}

	/**
	 * Changes "Posts" text in admin menu.
	 *
	 * @access public
	 * @since  2.1.4
	 * @return void
	 */
	public static function change_admin_menu_text() {
		global $menu;
		$menu[5][0] = \__('Blog Posts');
	}

	/**
	 * Changes core "Post" post type labels.
	 *
	 * @access public
	 * @since  2.1.4
	 * @return void
	 */
	public static function change_post_object_labels() {
		global $wp_post_types;
		if (!array_key_exists('post', $wp_post_types)) {
			return;
		}
		$labels = &$wp_post_types['post']->labels;
		$labels->name                  = \__('Blog Posts');
		$labels->singular_name         = \__('Blog Post');
		$labels->add_new_item          = \__('Add New Blog Post');
		$labels->all_items             = \__('All Blog Posts');
		$labels->archives              = \__('Blog Post Archives');
		$labels->attributes            = \__('Blog Post Attributes');
		$labels->edit_item             = \__('Edit Blog Post');
		$labels->insert_into_item      = \__('Insert into blog post');
		$labels->name                  = \__('Blog Posts');
		$labels->name_admin_bar        = \__('Blog Post');
		$labels->new_item              = \__('New Blog Post');
		$labels->not_found             = \__('No blog posts found');
		$labels->not_found_in_trash    = \__('No blog posts found in trash');
		$labels->search_items          = \__('Search Blog Posts');
		$labels->singular_name         = \__('Blog Post');
		$labels->uploaded_to_this_item = \__('Uploaded to this blog post');
		$labels->view_item             = \__('View Blog Post');
	}
}
