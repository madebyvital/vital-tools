<?php
if (! defined('ABSPATH')) {
	exit;
}

/**
 * Customizes the WordPress media library
 */
class Vital_Media_Library {

	/**
	 * Sets up the class functionality.
	 *
	 * @access public
	 * @since  1.0.0
	 * @return void
	 */
	public function __construct() {
		add_filter('media_view_settings', [$this, 'gallery_default_links']);
		add_filter('jpeg_quality', [$this, 'increase_jpg_compression']);
		add_filter('wp_handle_upload_prefilter', [$this, 'restrict_media_upload_file_size'], 10, 2);
		add_filter('wp_handle_upload_prefilter', [$this, 'restrict_media_upload_image_dimensions'], 10, 2);
	}

	/**
	 * Forces gallery images to link to file.
	 *
	 * @access public
	 * @since  1.0.0
	 * @param  array $settings List of media view settings.
	 * @return array Updates list of media view settings.
	 */
	public function gallery_default_links($settings) {
		$settings['galleryDefaults']['link'] = 'file';
		return $settings;
	}

	/**
	 * Increases JPG compression.
	 * Default is 90.
	 *
	 * @access public
	 * @since  1.0.0
	 * @param  int $quality Quality level between 0 (low) and 100 (high)
	 * @return int New quality level
	 */
	public function increase_jpg_compression($quality) {
		return 70;
	}

	/**
	 * Restrict file uploads in media library to set size
	 *
	 * @param array $file File being uploaded
	 * @return $file
	 */
	public static function restrict_media_upload_file_size($file) {
		$default_size = 10000; // 10MB default size limit

		// List of common MIME types
		$mime_types = [
			'image/jpeg',
			'image/png',
			'image/gif',
			'image/bmp',
			'image/webp',
			'image/svg+xml',
			'image/tiff',
			'video/mp4',
			'video/mpeg',
			'video/quicktime',
			'application/pdf',
		];

		// Early return for non-attachment uploads
		if (is_admin() && isset($_REQUEST['action']) && $_REQUEST['action'] !== 'upload-attachment') {
			return $file;
		}

		// Initialize size limits for all image types
		$size_limits = array_fill_keys($mime_types, $default_size);

		// Apply filter to allow developers to override size limits
		if (has_filter('media_upload_max_size')) {
			$size_limits = apply_filters('media_upload_max_size', $size_limits);
		}

		/** Example usage of filter
		* public static function vtl_restrict_max_upload_size( $size_limits ) {
		*     $size_limits['image/jpeg'] = 8000; // Limit JPGs to 8MB
		*     $size_limits['video/mp4'] = 50000; // Limit MP4 videos to 50MB
		*     return $size_limits;
		* }
		*/

		// If other restrictions are already applied, return
		if (isset($file['error']) && $file['error']) {
			return $file;
		}

		// Check if the uploaded file type has a size limit
		$type = $file['type'];
		$size_limit = $size_limits[$type] ?? null;

		// If the file type is not in the allowed MIME types, return
		if (!$size_limit) {
			return $file;
		}

		// Format size values to readable numbers
		$file_size = $file['size']; // File size in bytes
		$file_size_kb = $file_size / 1024; // Convert to kilobytes
		$max_size_mb = number_format($size_limit / 1000, 2); // Max size in megabytes
		$file_size_mb = number_format($file_size_kb / 1000, 2); // File size in megabytes

		// Check if the file size exceeds the maximum allowed size
		if ($file_size_kb > $size_limit) {
			return [
				'name'  => $file['name'],
				'error' => "File size is too large. Maximum file size for {$type} is {$max_size_mb}MB. Uploaded file size is {$file_size_mb}MB.",
			];
		}

		return $file;
	}

	/**
	 * Restrict image uploads in media library to set dimensions
	 *
	 * @param array $file File being uploaded
	 * @return $file
	 */
	public static function restrict_media_upload_image_dimensions($file) {
		// Default maximum dimensions (in pixels)
		$default_width = 10000;
		$default_height = 10000;

		// Supported image MIME types (excluding SVG, as dimensions aren't required)
		$image_mime_types = [
			'image/jpeg',
			'image/png',
			'image/gif',
			'image/bmp',
			'image/webp',
			'image/tiff',
		];

		// Early return for non-attachment uploads
		if (is_admin() && isset($_REQUEST['action']) && $_REQUEST['action'] !== 'upload-attachment') {
			return $file;
		}

		// Check for other errors in the file
		if (isset($file['error']) && $file['error']) {
			return $file;
		}

		// If file type is not an image or is an SVG, return
		if (!in_array($file['type'], $image_mime_types) || str_contains($file['type'], 'svg')) {
			return $file;
		}

		// Get maximum dimensions via filter
		$max_dimensions = [
			'width'  => $default_width,
			'height' => $default_height,
		];

		// Allow developers to customize dimensions via filter
		if (has_filter('media_upload_max_dimensions')) {
			$max_dimensions = apply_filters('media_upload_max_dimensions', $max_dimensions);
		}

		/** Example usage of filter
		* public static function vtl_restrict_max_image_dimensions( $dimensions ) {
		*     $dimensions['width'] = 5000; // Limit image width to 5000px
		*     return $dimensions;
		* }
		*/

		// Validate and fetch max width and height
		$max_width = isset($max_dimensions['width']) ? $max_dimensions['width'] : $default_width;
		$max_height = isset($max_dimensions['height']) ? $max_dimensions['height'] : $default_height;

		// Get actual image dimensions
		$file_dimensions = getimagesize($file['tmp_name']);
		if ($file_dimensions) {
			$file_width = $file_dimensions[0];
			$file_height = $file_dimensions[1];
		} else {
			return $file;
		}

		// Check if width exceeds the limit
		if ($max_width && $file_width > $max_width) {
			return [
				'name'  => $file['name'],
				'error' => "Image dimensions are too large. Maximum width is {$max_width}px. Uploaded image width is {$file_width}px.",
			];
		}

		// Check if height exceeds the limit
		if ($max_height && $file_height > $max_height) {
			return [
				'name'  => $file['name'],
				'error' => "Image dimensions are too large. Maximum height is {$max_height}px. Uploaded image height is {$file_height}px.",
			];
		}

		return $file;
	}

}
