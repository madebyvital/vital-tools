<?php
if (! defined('ABSPATH')) {
	exit;
}

/**
 * Initializates all plugin classes.
 *
 * @since   1.0.0
 */
class Vital_Tools_Init {

	/**
	 * Sets up the class functionality.
	 *
	 * @access public
	 * @since  1.0.0
	 * @return void
	 */
	public function __construct() {
		$acf = new Vital_Tools_ACF();
		$admin_blog_rename = new Vital_Admin_Blog_Rename();
		$admin_render = new Vital_Admin_Render();
		$admin_users = new Vital_Admin_Users();
		$development = new Vital_Development();
		$gravity_forms = new Vital_Gravity_Forms();
		$media_library = new Vital_Media_Library();
		$oemved = new Vital_Oembed();
		$page_cleanup = new Vital_Page_Cleanup();
		$page_render = new Vital_Page_Render();
		$performance = new Vital_Performance();
		$rename_blog = new Vital_Rename_Blog();
		$search = new Vital_Search();
		$searchwp = new Vital_SearchWP();
		$theme_setup = new Vital_Theme_Setup();
		$wordpress = new Vital_WordPress();
		$yoast = new Vital_Yoast();

		if ((defined('WP_DEBUG') && WP_DEBUG === true) && is_admin()) {
			$admin_reveal_ids = new Vital_Admin_Reveal_Ids();
		}
	}
}
