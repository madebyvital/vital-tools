<?php
class DisableFileEditors {
	public function __construct() {
		// Adding action to disable editors
		add_action('init', array($this, 'disable_file_editors'));
	}

	public function disable_file_editors() {
		// Define constants to disable theme and plugin editors
		if (!defined('DISALLOW_FILE_EDIT')) {
			define('DISALLOW_FILE_EDIT', true);
		}
	}
}

// Instantiate the class to apply the changes
new DisableFileEditors();
